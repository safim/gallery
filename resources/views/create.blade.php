<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css">
    <title>Upload</title>
</head>

<body>

    <div class="container container-fluid mt-5">
        <h2>Upload you image here</h2>
        <form action="{{ route('images.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <input type="file" class="form-control w-auto" id="image" name="image"
                    aria-describedby="imageHelp">
                <div id="eHelp" class="form-text">Save something nice</div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>



</body>

</html>
